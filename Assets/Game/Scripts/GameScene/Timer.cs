﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    // Variables  to store hours,minutes and seconds
    int hour = 0;           
    int minute = 0;
    float seconds = 0;

    void Update()
    {
        // Update seconds and change no of minutes and hours based on it
        seconds += Time.deltaTime;

        if (minute >= 60)
        {
            hour++;
            minute = 0;
        }

        if(seconds >= 60)
        {
            minute++;
            seconds = 0;
        }

        //Update UI Texty
        this.GetComponent<Text>().text = "Time : " + hour + ":" + minute + ":" + Convert.ToInt32(seconds);
    }
}
