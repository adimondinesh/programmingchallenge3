﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class has all the variables associated with the mobile touch controls and V-Pad (virtual joystick)

[System.Serializable]
public class JoystickControl
{
    public Transform outerCirlce;           // Outer circle of Vpad
    public Transform innerCirlce;           // Inner circle of Vpad
    public float threshold = 50;            // Max distance the inner circle can go

    public bool touch = false;              // This checks whether user is touching the screen or not
    public Vector2 pointA;                  // The start position when the first touch was recorded inside the V-pad
    public Vector2 pointB;                  // The end position when the touch ended inside the V-pad
    public Vector2 StartPoint = Vector2.zero;   // The start position when touch started
    public Vector2 EndPoint = Vector2.zero;     // The end position when touch 

    public float circleRadius;              // The range upto which the innerCircle of Vpad can move

    public bool isSwipe = false;            // Whether a swipe gesture is detected
}

public class NinjaController : MonoBehaviour
{
    public JoystickControl joystickControl;

    public float speed = 5.0f;                  // Speed of Ninja
    public float movementThreshold = 0.5f;      // Movement Threshold
    public Transform spawnPos;                  // Ninja Spawn Position
    public GameObject fireBall;
    public float fireBallSpeed = 2;             // Speed of Fireball

    private Animator animator;                  // Ninja Animator

    public Vector3 inputDirection;              // Input Direction from Keyboard
    private Rigidbody2D rb2D;                   // Rigidbody of Ninja

    //Enum states to know whihc direction the Ninja is facing
    public enum NinjaDirection
    {
        Front,
        Left,
        Right,
        Back
    };

    public NinjaDirection ninjaDirection = NinjaDirection.Front;

    // Set the range for Vpad
    private void Awake()
    {
        joystickControl.circleRadius = (joystickControl.outerCirlce.GetComponent<RectTransform>().rect.width * 0.5f) - joystickControl.threshold;
    }
    void Start()
    {
        this.transform.position = spawnPos.position;
        rb2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        // get the input
        inputDirection = getInput();

        // Check if the magnitude is greater than the threshold

        if (inputDirection.magnitude > movementThreshold)
        {
            rb2D.velocity = new Vector2(inputDirection.x * speed, inputDirection.y * speed);
            animator.SetFloat("inputX", inputDirection.x);
            animator.SetFloat("inputY", inputDirection.y);
            animator.SetBool("Walking", true);
        }

        else
        {
            rb2D.velocity = Vector2.zero;
            animator.SetBool("Walking", false);
        }

        // Update the direction states of Ninja based on input Direction
        #region Ninja Direction

        if (inputDirection.x > 0.5 && (inputDirection.y > -0.5 && inputDirection.y < 0.5))
        {
            ninjaDirection = NinjaDirection.Right;
        }
        if (inputDirection.x < -0.5 && (inputDirection.y > -0.5 && inputDirection.y < 0.5))
        {
            ninjaDirection = NinjaDirection.Left;
        }
        if (inputDirection.y < -0.5 && (inputDirection.x > -0.5 && inputDirection.x < 0.5))
        {
            ninjaDirection = NinjaDirection.Front;
        }
        if (inputDirection.y > 0.5 && (inputDirection.x > -0.5 && inputDirection.x < 0.5))
        {
            ninjaDirection = NinjaDirection.Back;
        }

        #endregion

        // If Swipe motion is detected than shoot fireball
        if (joystickControl.isSwipe)
        {
            animator.SetBool("Attack", true);
            Instantiate(fireBall, this.transform.position, this.transform.rotation);
            joystickControl.isSwipe = false;
        }
    }

    // On Colliding with Fire traps Respawn Player at SpawnPoint
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Fire"))
        {
            this.transform.position = spawnPos.position;
        }
    }

    // Take inputs from user on a touch screen
    Vector2 getInput()
    {
        // If user has touched screen
        if (Input.touchCount > 0)
        {
            // If the touch position is in the Vpad range then mark point A
            if (Vector3.Distance(Input.GetTouch(0).position, joystickControl.innerCirlce.position) < joystickControl.circleRadius && !joystickControl.touch)
            {
                joystickControl.pointA = Input.GetTouch(0).position;
                joystickControl.touch = true;
            }

            // If the touch position is outside Vpad range then mark StartPoint
            else
            {
                if(Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    joystickControl.StartPoint = Input.GetTouch(0).position;
                }
            }

            // If the StartPoint and Endpoint of touch position is not same then it's a swipe gesture
            if(Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                joystickControl.EndPoint = Input.GetTouch(0).position;
                if (joystickControl.StartPoint != Vector2.zero && joystickControl.StartPoint != joystickControl.EndPoint)
                {
                    joystickControl.isSwipe = true;
                    joystickControl.StartPoint = Vector2.zero;
                }
            }
        }
        else
        {
            joystickControl.touch = false;
        }

        // Constantantly store point B as the current touch position when point A has been marked
        if (joystickControl.touch)
        {
            joystickControl.pointB = Input.GetTouch(0).position;

            // If point B is outside the range of Vpad then normalize its value and then let the inner circle of Vpad track point B
            if (Vector3.Distance(joystickControl.outerCirlce.position, joystickControl.pointB) > joystickControl.circleRadius)
            {
                Vector2 origin = joystickControl.outerCirlce.position;
                joystickControl.innerCirlce.position = (joystickControl.pointB - origin).normalized * joystickControl.circleRadius + origin;
            }

            // If point B is within Vpad range then iner Cirle of Vpad will directly track point B
            else
            {
                joystickControl.innerCirlce.position = joystickControl.pointB;
            }

            Vector2 offset = joystickControl.pointB - joystickControl.pointA;
            Vector2 direction = Vector2.ClampMagnitude(offset, 1.0f);
            return (direction);
        }

        // Reset inner circle position when touch has ended
        else
        {
            joystickControl.innerCirlce.position = joystickControl.outerCirlce.position;
            return Vector3.zero;
        }
    }
}
