﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTrigger : MonoBehaviour
{
    public AudioClip fireSound;                 // Sound when fire is triggerred
    public AudioClip alarmSound;                // Sound when you enter trigger area
    public float waitTime = 3.0f;               // wait time to trigger fire
    public GameObject[] fireTraps;              // List of Fire Objects to be triggered
    AudioSource trapAudio;                      // Audio source to play sounds
    float timer = 0;                            // Timer variable
    bool startFire = false;                     // Triggers fire trap
    bool startTimer = false;                    // Triggers timer

    void Start()
    {
        trapAudio = this.GetComponent<AudioSource>();
    }

    void Update()
    {
        // If Timer trigger is true then start checking time and trigger fire with fire sound when timer reaches waitTime
        if (startTimer)
        {
            timer += Time.deltaTime;

            if (timer >= waitTime)
            {
                trapAudio.PlayOneShot(fireSound);
                startFire = true;
                startTimer = false;
            }
        }

        // If Fire triiger is true then enable all Fire objects
        if (startFire)
        {
            for (int i = 0; i < fireTraps.Length; i++)
            {
                fireTraps[i].SetActive(true);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // If player enters trigger Area of the Trap then Timer is triggered and Alarm sound is played

        if (collision.gameObject.CompareTag("Player") && startFire == false)
        {
            trapAudio.PlayOneShot(alarmSound);
            startTimer = true;
        }
    }
}
