﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinCollect : MonoBehaviour
{
    public Text scoreText;          // Score Text HUD
    Animator coinAnim;              // Coin Animator
    int score = 0;                  // Score

    void Update()
    {
        // Update HUD Score
        scoreText.text = "Score : " + score;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {   
        // If player collides with coin increment score and play CoinCollect animation
        if (collision.gameObject.name == "Coin")
        {
            coinAnim = collision.gameObject.GetComponent<Animator>();
            coinAnim.SetTrigger("coinCollect");
            score++;
        }
    }
}
