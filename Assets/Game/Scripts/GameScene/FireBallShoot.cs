﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallShoot : MonoBehaviour
{
    Rigidbody2D rb;                         // Fire ball RigidBody
    NinjaController ninja;                  // Ninja Script

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        ninja = GameObject.Find("Ninja").GetComponent<NinjaController>();

        // Check Ninja's direction and Add velocity to fireball in the respective direction

        if(ninja.ninjaDirection == NinjaController.NinjaDirection.Back)
        {
            rb.velocity = new Vector2(0, ninja.fireBallSpeed);
        }

        if (ninja.ninjaDirection == NinjaController.NinjaDirection.Front)
        {
            rb.velocity = new Vector2(0, -ninja.fireBallSpeed);
        }

        if (ninja.ninjaDirection == NinjaController.NinjaDirection.Left)
        {
            rb.velocity = new Vector2(-ninja.fireBallSpeed, 0);
        }

        if (ninja.ninjaDirection == NinjaController.NinjaDirection.Right)
        {
            rb.velocity = new Vector2(ninja.fireBallSpeed, 0);
        }

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Fireball destroys on collision with wall

        if (collision.gameObject.CompareTag("Wall"))
        {
            Destroy(this.gameObject);
        }
    }
}
