﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfterCoinCollectStateBehaviour : StateMachineBehaviour
{
    // Destroy coin after coin collect animation is over
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Destroy(animator.gameObject);
    }
}
