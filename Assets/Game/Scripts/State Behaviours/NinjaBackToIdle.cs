﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaBackToIdle : StateMachineBehaviour
{
    // Sets the Ninja back to Idle position after it completes the attack animation
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("Attack", false);
    }
}
